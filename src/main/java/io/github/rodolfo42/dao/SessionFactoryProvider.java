package io.github.rodolfo42.dao;


import io.github.rodolfo42.business.Filme;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.ComponentFactory;

@Component
@ApplicationScoped
public class SessionFactoryProvider implements ComponentFactory<SessionFactory> {
	
	private SessionFactory factory;
	private final Logger LOG = LoggerFactory.getLogger(getClass());
	
	@PostConstruct
	public void buildSessionFactory() {
		AnnotationConfiguration configuration = new AnnotationConfiguration();
		try {
			configuration.configure();
			this.factory = configuration.buildSessionFactory();
		} catch (Exception e) {
			LOG.error("erro ao construir o SessionFactory", e);
		}
		
		
		// popular os filmes na criação do factory
		// já que este provider tem escopo de aplicação
		// e não há CRUD de filmes
		List<Filme> filmes = new ArrayList<Filme>();
		filmes.add(new Filme("The Dark Knight"));
		filmes.add(new Filme("The Big Lebowski"));
		filmes.add(new Filme("No Country For Old Men"));
		filmes.add(new Filme("La Vita è Bella"));
		filmes.add(new Filme("Gran Torino"));
		
		Session sess = this.factory.openSession();
		Transaction tx = sess.beginTransaction();
		tx.begin();
		for (Filme filme : filmes) {
			sess.save(filme);
		}
		tx.commit();
		sess.close();
	}
	
	public SessionFactory getInstance() {
		return this.factory;
	}
	
	@PreDestroy
	public void fechar() {
		try {
			this.factory.close();
		} catch(Exception e) {
			LOG.error("erro ao fechar o SessionFactory", e);
		}
	}
}