package io.github.rodolfo42.dao;

import io.github.rodolfo42.business.Filme;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.caelum.vraptor.ioc.Component;

@Component
public class FilmeDao {

	private static Class<Filme> clazz = Filme.class;
	private Session session;

	public FilmeDao(Session session) {
		this.session = session;
	}

	public ArrayList<Filme> getFilmes() {
		return (ArrayList<Filme>) session.createCriteria(clazz).list();
	}

	public Integer save(Filme filme) {
		Transaction tx = session.beginTransaction();
		Integer id = (Integer) session.save(filme);
		tx.commit();
		return id;
	}

	public Filme getFilme(Integer id) {
		return (Filme) session.load(clazz, id);
	}

	public ArrayList<Filme> getFilmesOrdenadosPeloRanking() {
		ArrayList<Filme> filmesOrdenados = getFilmes();
		Collections.sort(filmesOrdenados);
		return filmesOrdenados;
	}
}
