package io.github.rodolfo42.dao;

import io.github.rodolfo42.business.Usuario;

import java.io.Serializable;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.caelum.vraptor.ioc.Component;

@Component
public class UsuarioDao {

	private static Class<Usuario> clazz = Usuario.class;
	private Session session;

	public UsuarioDao(Session session) {
		this.session = session;
	}

	public Integer save(Usuario usuario) {
		Transaction tx = session.beginTransaction();
		Integer id = (Integer) session.save(usuario);
		tx.commit();
		return id;
	}
	
	public Usuario getUsuario(Integer id) {
		return (Usuario) session.load(clazz, id);
	}
}
