package io.github.rodolfo42.controller;

import io.github.rodolfo42.business.Filme;
import io.github.rodolfo42.business.Ranking;
import io.github.rodolfo42.business.Usuario;
import io.github.rodolfo42.dao.FilmeDao;
import io.github.rodolfo42.dao.UsuarioDao;

import java.util.ArrayList;
import java.util.Collections;

import javax.annotation.PostConstruct;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;

@Resource
public class RankingController extends BaseController {

	private Result result;
	private Ranking ranking;
	private FilmeDao filmeDao;
	private UsuarioDao usuarioDao;

	public RankingController(Result result, Ranking ranking, FilmeDao filmeDao,
			UsuarioDao usuarioDao) {
		this.result = result;
		this.ranking = ranking;
		this.filmeDao = filmeDao;
		this.usuarioDao = usuarioDao;
	}

	@PostConstruct
	public void initFilmes() {
		// somente inicializa o ranking com os filmes
		// caso ainda não tenha iniciado o registro dos votos
		if (!this.ranking.isIniciado()) {
			this.ranking.setFilmes(filmeDao.getFilmes());
		}
	}

	@Get
	@Path("/")
	public void votar() {
		if (ranking.isFinalizado()) {
			result.redirectTo(this).dados();
		} else {
			result.include("perguntaRanking", ranking.getPergunta());
		}
	}

	@Post
	@Path("/")
	public void votar(Integer filmeId) {
		if (filmeId == null) {
			result.redirectTo(this).votar();
		} else {
			Filme filme = filmeDao.getFilme(filmeId);

			ranking.getPergunta().responder(filme);
			LOG.debug("voto: " + filme.getTitulo());

			if (!ranking.isFinalizado()) {
				result.redirectTo(this).votar();
			} else {
				result.redirectTo(this).dados();
			}
		}
	}

	@Get
	@Path("/dados")
	public ArrayList<Filme> dados() {
		if (!ranking.isFinalizado()) {
			result.redirectTo(this).votar();
		}
		return ranking.getResultado();
	}

	@Post
	@Path("/concluir")
	public void concluir(Usuario usuario) {
		if (!ranking.isFinalizado()) {
			result.redirectTo(this).votar();
		}
		
		// registra o ranking do usuário
		usuario.registrarRanking(ranking);
		usuarioDao.save(usuario);
		
		result.redirectTo(this).home();
	}

	
	@Get
	@Path("/home")
	public void home() {
		if(ranking.isFinalizado()) {
			// incluir ranking do usuario na tela de resultado final
			result.include("rankingUsuario", ranking.getResultado());
			ranking.reset();
		}
		// ranking geral de todos os usuários
		ArrayList<Filme> rankingGeral = filmeDao.getFilmesOrdenadosPeloRanking();
		result.include("rankingGeral", rankingGeral);
	}
}
