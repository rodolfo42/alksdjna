package io.github.rodolfo42.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseController {
	
	protected static Logger LOG = LoggerFactory.getLogger(BaseController.class);
	
}