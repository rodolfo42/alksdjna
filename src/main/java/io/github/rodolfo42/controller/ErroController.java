package io.github.rodolfo42.controller;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;

@Resource
public class ErroController extends BaseController {

	@Get
	@Path("/erro/500")
	public void erro500() {

	}

	@Get
	@Path("/erro/404")
	public void erro404() {

	}
}
