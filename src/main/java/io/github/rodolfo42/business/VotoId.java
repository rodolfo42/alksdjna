package io.github.rodolfo42.business;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class VotoId implements Serializable {
	
	private static final long serialVersionUID = 5055627994778625407L;

	@ManyToOne
	private Usuario usuario;
	
	@ManyToOne
	private Filme filme;

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Filme getFilme() {
		return filme;
	}

	public void setFilme(Filme filme) {
		this.filme = filme;
	}
}
