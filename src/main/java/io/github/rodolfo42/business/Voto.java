package io.github.rodolfo42.business;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Transient;

/**
 * Representa um voto em uma determinada posição em um ranking de um usuário.
 * São usados coletivamente para calcular a soma total do ranking de um
 * determinado filme
 * 
 * @author rodolfo42
 */
@Entity
@AssociationOverrides({
	@AssociationOverride(name = "pk.filme", joinColumns = @JoinColumn(name = "filme_id")),
	@AssociationOverride(name = "pk.usuario", joinColumns = @JoinColumn(name = "usuario_id"))
})
public class Voto {
	
	@EmbeddedId
	private VotoId pk = new VotoId();

	private int posicaoRanking;

	Voto() {
	}

	public Voto(Usuario usuario, Filme filme, int posicaoRanking) {
		this.posicaoRanking = posicaoRanking;
		setUsuario(usuario);
		setFilme(filme);
	}
	
	public VotoId getPk() {
		return pk;
	}

	public void setPk(VotoId pk) {
		this.pk = pk;
	}

	@Transient
	public Usuario getUsuario() {
		return getPk().getUsuario();
	}

	public void setUsuario(Usuario usuario) {
		getPk().setUsuario(usuario);
	}

	@Transient
	public Filme getFilme() {
		return getPk().getFilme();
	}

	public void setFilme(Filme filme) {
		this.getPk().setFilme(filme);
	}

	public int getPosicaoRanking() {
		return posicaoRanking;
	}

	public void setPosicaoRanking(int posicaoRanking) {
		this.posicaoRanking = posicaoRanking;
	}
}
