package io.github.rodolfo42.business;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.SessionScoped;

/**
 * Classe que representa a lógica de ranking.
 * 
 * A lógica de ranking é baseada em uma árvore binária (não-balanceada). Existe
 * uma lista de filmes a serem rankeados (os nós da árvore), e na medida em que
 * cada filme da lista vai sendo inserido na árvore, existem comparações que são
 * feitas com cada filme para definir a ordem na qual o filmes inseridos deverão
 * ficar.
 * 
 * Estas comparações serão então efetuadas em forma de pergunta ao usuário (ou
 * em consulta á uma ordem de preferência pré-definida, no caso dos testes).
 * 
 * No final, o ranking será extraído da árvore listando os nós em ordem natural
 * (Esquerda-Raiz-Direita)
 * 
 * @author rodolfo42
 */
@SessionScoped
@Component
public class Ranking {

	protected static Logger LOG = LoggerFactory.getLogger(Ranking.class);

	/**
	 * Representa um nó da árvore. Cada nó tem dois filhos esquerdo e direito e
	 * são ordenados pelo ranking de melhor e pior, respectivamente (ou 1o e 5o
	 * lugar).
	 * 
	 * @author rodolfo42
	 */
	private class RankingNo {
		private final Filme filme;
		private RankingNo noEsquerdo = null;
		private RankingNo noDireito = null;

		public RankingNo(Filme filme) {
			this.filme = filme;
		}
	}

	/**
	 * Lista total de filmes participando do ranking
	 */
	private List<Filme> filmes;

	/**
	 * Nó raíz da árvore. Usado para determinar o ponto de partida e para
	 * retornar a ordem natural
	 */
	private RankingNo raiz;

	/**
	 * Indica a partir de qual filme o ranking ainda esta para ser definido
	 */
	private int indiceAtual;

	/**
	 * Representa o nó em que a pergunta está pendente de comparação
	 */
	private RankingNo noAtual;

	/**
	 * Filme a ser inserido na árvore. Está a ser comparado com o noAtual
	 */
	private Filme candidato;

	/**
	 * Indica se o ranking já começou a ser definido
	 */
	private boolean isIniciado = false;

	public Ranking() {
	}

	/**
	 * O construtor já com a lista de filmes para o ranking
	 * 
	 * @param filmes
	 */
	public Ranking(List<Filme> filmes) {
		setFilmes(filmes);
	}

	/**
	 * Inicializa o ranking populando a lista de filmes a serem classificados
	 * 
	 * @param filmes
	 */
	public void setFilmes(List<Filme> filmes) {
		this.isIniciado = true;
		this.filmes = filmes;
		this.raiz = new RankingNo(filmes.get(0));
		indiceAtual = 1;
		prepararProximaPergunta(this.raiz, filmes.get(indiceAtual));
	}

	/**
	 * Prepara os filmes para a próxima pergunta
	 * 
	 * @param noAtual
	 *            O nó já existente na árvore
	 * @param candidato
	 *            O filme a ser inserido (comparado)
	 */
	private void prepararProximaPergunta(RankingNo noAtual, Filme candidato) {
		this.noAtual = noAtual;
		this.candidato = candidato;
	}

	/**
	 * Retorna um objeto que encapsula os dois filmes e permite resposta
	 * 
	 * @return Pergunta
	 */
	public PerguntaRanking getPergunta() {
		return new PerguntaRanking(noAtual.filme, candidato, this);
	}

	/**
	 * Registra a resposta para a pergunta preparada por
	 * {@link #prepararProximaPergunta(RankingNo, Filme)}
	 * 
	 * @param resposta
	 */
	public void registraResposta(Filme resposta) {
		// se candidato é melhor que pai
		if (resposta.equals(candidato)) {
			// caso pai ja tenha filho esquerdo, comparar novamente
			if (noAtual.noEsquerdo != null) {
				prepararProximaPergunta(noAtual.noEsquerdo, candidato);
			} else {
				// caso contrário, já insere o candidato como filho esquerdo
				// (melhor)
				noAtual.noEsquerdo = new RankingNo(candidato);

				indiceAtual++;
				if (!isFinalizado())
					prepararProximaPergunta(raiz, filmes.get(indiceAtual));
			}
		} else {
			// senao, faz o inverso
			if (noAtual.noDireito != null) {
				// compara novamente se já houver um filho
				prepararProximaPergunta(noAtual.noDireito, candidato);
			} else {
				// senão, inserir no lado direito (pior)
				noAtual.noDireito = new RankingNo(candidato);

				indiceAtual++;
				if (!isFinalizado())
					prepararProximaPergunta(raiz, filmes.get(indiceAtual));
			}
		}
	}

	/**
	 * Retorna se o ranking já foi finalizado, ou seja, se todos os filmes já
	 * foram devidamente classificados
	 * 
	 * @return
	 */
	public boolean isFinalizado() {
		return filmes.size() == indiceAtual;
	}
	
	/**
	 * Retorna se o ranking já foi iniciado (os filmes já foram inicializados e
	 * foi processada a primeira pergunta a ser feita)
	 * 
	 * @return
	 */
	public boolean isIniciado() {
		return this.isIniciado;
	}

	/**
	 * Retorna o resultado da classificação dos filmes
	 * 
	 * @return
	 */
	public ArrayList<Filme> getResultado() {
		return getFilmesEmOrdem(null);
	}
	
	/**
	 * Re-inicia o ranking e zera os dados de classificação coletados até o momento
	 */
	public void reset() {
		setFilmes(filmes);
	}

	/**
	 * Método recursivo para retornar em ordem natural os nós da árvore
	 * (Esquerda-Raiz-Direita)
	 * 
	 * @param raizOrdem
	 * @return os filmes em ordem natural na árvore
	 */
	private ArrayList<Filme> getFilmesEmOrdem(RankingNo raizOrdem) {
		ArrayList<Filme> ordem = new ArrayList<Filme>();
		if (raizOrdem == null) {
			raizOrdem = raiz;
		}

		// esquerda
		if (raizOrdem.noEsquerdo != null) {
			ordem.addAll(getFilmesEmOrdem(raizOrdem.noEsquerdo));
		}

		// raíz
		ordem.add(raizOrdem.filme);

		// direita
		if (raizOrdem.noDireito != null) {
			ordem.addAll(getFilmesEmOrdem(raizOrdem.noDireito));
		}

		return ordem;
	}
}