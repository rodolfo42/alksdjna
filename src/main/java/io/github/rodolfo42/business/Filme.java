package io.github.rodolfo42.business;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.validator.NotEmpty;

@Entity
public class Filme implements Comparable<Filme> {

	@Id
	@GeneratedValue
	@Column(name = "filme_id")
	private Integer id;

	@NotEmpty
	private String titulo;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.filme", cascade = CascadeType.ALL)
	private Set<Voto> votos = new HashSet<Voto>(0);

	Filme() {
	}

	public Filme(String titulo) {
		this.titulo = titulo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Set<Voto> getVotos() {
		return votos;
	}

	public void setVotos(Set<Voto> votos) {
		this.votos = votos;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		} else if (!obj.getClass().equals(getClass())) {
			return false;
		}

		Filme other = (Filme) obj;
		if (getId() != null) {
			if (other.getId() == null) {
				return false;
			} else if (!other.getId().equals(getId())) {
				return false;
			}
		} else if (other.getId() != null) {
			return false;
		}

		if (getTitulo() != null) {
			if (other.getTitulo() == null) {
				return false;
			} else if (!other.getTitulo().equals(getTitulo())) {
				return false;
			}
		} else if (other.getTitulo() != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return getTitulo().hashCode();
	}

	@Override
	public String toString() {
		return getTitulo();
	}

	public int getPosicaoRankingGeral() {
		int posicaoRankingGeral = 0;
		for (Voto voto : getVotos()) {
			posicaoRankingGeral += voto.getPosicaoRanking();
		}

		return posicaoRankingGeral;
	}

	@Override
	public int compareTo(Filme o) {
		int result = 0;
		if(o != null) {
			result = (o.getPosicaoRankingGeral() - this.getPosicaoRankingGeral());
		}
		return result;
	}
}