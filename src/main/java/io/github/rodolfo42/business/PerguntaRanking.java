package io.github.rodolfo42.business;


/**
 * Classe simples para encapsular os dois filmes envolvidos em uma pergunta, bem como sua resposta.
 * 
 * @author rodolfo42
 */
public class PerguntaRanking {

	private final Ranking ranking;
	private final Filme primeiroFilme;
	private final Filme segundoFilme;
	
	private Filme resposta = null;

	public PerguntaRanking(Filme primeiroFilme, Filme segundoFilme, Ranking ranking) {
		this.primeiroFilme = primeiroFilme;
		this.segundoFilme = segundoFilme;
		this.ranking = ranking;
	}

	public Filme getPrimeiroFilme() {
		return primeiroFilme;
	}

	public Filme getSegundoFilme() {
		return segundoFilme;
	}
	
	public Filme getResposta() {
		return resposta;
	}
	
	public void responder(Filme filme) {
		if(filme.equals(primeiroFilme) || filme.equals(segundoFilme)) {
			resposta = filme;
			ranking.registraResposta(resposta);
		} else {
			throw new RuntimeException("Resposta inválida");
		}
	}
}