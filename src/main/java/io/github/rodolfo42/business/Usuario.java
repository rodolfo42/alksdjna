package io.github.rodolfo42.business;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.validator.Email;
import org.hibernate.validator.NotEmpty;

@Entity
public class Usuario {

	@Id
	@GeneratedValue
	@Column(name = "usuario_id")
	private Integer id;

	@NotEmpty
	private String nome;

	@NotEmpty
	@Email
	private String email;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.usuario", cascade = CascadeType.ALL)
	private Set<Voto> votos = new HashSet<Voto>(0);

	Usuario() {
	}

	public Usuario(String nome, String email) {
		this.nome = nome;
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void registrarRanking(Ranking ranking) {
		ArrayList<Filme> ordemPreferencia = ranking.getResultado();
		int posicaoRanking = ordemPreferencia.size();
		votos = new HashSet<Voto>();

		for (Filme filme : ordemPreferencia) {
			votos.add(new Voto(this, filme, posicaoRanking));
			posicaoRanking--;
		}
	}

	public Set<Voto> getVotos() {
		return votos;
	}

	public void setVotos(Set<Voto> votos) {
		this.votos = votos;
	}
}
