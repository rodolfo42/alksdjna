<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>
	<decorator:title default="Vote no Filme" /></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<c:url value="/foundation/css/normalize.css" />" />
	<link rel="stylesheet" type="text/css" href="<c:url value="/foundation/css/foundation.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/jquery.qtip.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/app.css" />" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
	
	<script type="text/javascript" src="<c:url value="/js/jquery.min.js"/>"></script>
	
	<!-- necessário para o qTip -->
	<script type="text/javascript" src="<c:url value="/js/jquery-migrate.min.js"/>"></script>
	
	<script type="text/javascript" src="<c:url value="/js/jquery.qtip.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/js/jquery.validate.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/foundation/js/foundation.min.js"/>"></script>
</head>
<body>
	<div class="container">
		<div class="panel callout">
			<div class="row">
				<div class="small-10 column">
					<h1>VoteNoFilme App</h1>
				</div>
				<div class="small-2 column">
					<a class="text-right" href="<c:url value="/home"/>">Home</a>
				</div>
			</div>			
		</div>
		<decorator:body />
	</div>
</body>
</html>