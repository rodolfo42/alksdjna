<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="container">
	<div class="row">
		<div class="large-8 large-centered column">
			<h1>Oops.. página não encontrada!</h1>
			<div class="alert-box radius secondary">
				<h4>Erro 404!</h4>
				<p>
					A página que você tentou acessar não existe ou foi movida para outro
					local.<br /> Tente acessar a página inicial <a href="<c:url value="/" />">clicando aqui</a>
				</p>
			</div>
		</div>
	</div>
</div>