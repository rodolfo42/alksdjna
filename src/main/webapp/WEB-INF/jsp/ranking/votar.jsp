<%@page import="io.github.rodolfo42.business.PerguntaRanking"%>
<%@page import="io.github.rodolfo42.business.Filme"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<body>
	
		<div class="row">
			<div class="large-8 large-centered column">
				<div class="panel">
					<form method="POST" action="<c:url value="/" />">
						<p>Escolha seu filme abaixo</p>
						
						<div class="row">
							<div class="large-6 column">
								<div class="filme">
									<input type="radio" style="display: none;" 
										name="filmeId" value="${perguntaRanking.primeiroFilme.id}" />
									<div class="titulo">${perguntaRanking.primeiroFilme.titulo}</div>
									<div class="pic">
										<img src="<c:url value="/img/filmes/${perguntaRanking.primeiroFilme.id}.jpg" />" />
									</div>
								</div>
							</div>
							<div class="large-6 column">
								<div class="filme">
									<input type="radio" style="display: none;" 
										name="filmeId" value="${perguntaRanking.segundoFilme.id}" />
									<div class="titulo">${perguntaRanking.segundoFilme.titulo}</div>
									<div class="pic">
										<img src="<c:url value="/img/filmes/${perguntaRanking.segundoFilme.id}.jpg" />" />
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="large-12 column">
								<button type="submit" class="success radius expand">Votar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	<script type="text/javascript">
	// alterna para o radio correspondente ao filme selecionado
	$(function(){
		var className = 'selecionado';
		$('.filme').bind('click', function() {
			$(this).find(':input').attr('checked', true);
			$('.filme').removeClass(className);
			$(this).addClass(className);
		});
	});
	</script>
</body>
</html>