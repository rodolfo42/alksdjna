<%@page import="io.github.rodolfo42.business.Filme"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>Situação do ranking geral</title>
</head>
<body>
	<div class="row">
		<div class="large-8 large-centered column">
			<h2 class="text-center">Raking geral</h2>
			<table class="expand">
				<thead>
					<tr>
						<td width="30">Posição</td>
						<td>Filme</td>
						<td>Quantidade total de votos</td>
						<td>Soma das posições</td>
					</tr>
				</thead>
				<tbody>
				<c:forEach items="${rankingGeral}" var="filme" varStatus="i">
					<tr>
						<td class="text-center">${ i.count }</td>
						<td valign="middle">
							<img height="25" width="25" 
								src="<c:url value="/img/filmes/${ filme.id }.jpg" />" /> ${ filme.titulo }
						</td>
						<td>
							${ filme.votos.size() }
						</td>
						<td>
							${ filme.posicaoRankingGeral }
						</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
		<c:choose>
			<c:when test="${ not empty rankingUsuario }">
				<div class="large-6 large-centered column">
					<h2 class="text-center">Seu ranking</h2>
					<table class="expand">
						<thead>
							<tr>
								<td width="30">Posição</td>
								<td>Filme</td>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${rankingUsuario}" var="filme" varStatus="i">
							<tr>
								<td class="text-center">${ i.count }</td>
								<td valign="middle">
									<img height="25" width="25" 
										src="<c:url value="/img/filmes/${ filme.id }.jpg" />" /> ${ filme.titulo }
								</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			</c:when>
			<c:otherwise>
				<div class="large-4 large-centered column">
					<a href="<c:url value="/" />" class="button success expand">VOTAR</a>
				</div>
			</c:otherwise>
		</c:choose>
	</div>
</body>
</html>