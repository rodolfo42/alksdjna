<%@page import="io.github.rodolfo42.business.Filme"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>Informe seus dados</title>
</head>
<body>
	<div class="row">
		<div class="large-5 large-centered column">
			<h2>Insira seus dados</h2>
			<p>Para concluir seu voto, é necessário fornecer os dados abaixo:</p>
			<form id="DadosForm" method="POST" action="<c:url value="/concluir" />">
				<div class="panel">
					<div class="row">
						<div class="column">
							<input placeholder="Nome completo" type="text" class="required expand" name="usuario.nome" />
						</div>
					</div>
					<div class="row">
						<div class="column">
							<input placeholder="e-mail" type="text" class="required email expand" name="usuario.email" />
						</div>
					</div>
					<div class="row">
						<div class="column">
							<button type="submit" class="expand success">Concluir</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	
	<div class="row">
		<div class="large-6 large-centered column">
			<h2 class="text-center">Preview do seu ranking</h2>
			<table class="expand">
				<thead>
					<tr>
						<td width="30">Posição</td>
						<td>Filme</td>
					</tr>
				</thead>
				<tbody>
				<c:forEach items="${filmeList}" var="filme" varStatus="i">
					<tr>
						<td class="text-center">${ i.count }</td>
						<td valign="middle"><img height="25" width="25" src="<c:url value="/img/filmes/${filme.id}.jpg" />" /> ${ filme.titulo }</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	
	<script type="text/javascript">
	$(function(){
		$('#DadosForm').validate({
			errorClass: 'error',
			validClass: 'valid',
			messages: {
				'usuario.nome': {
					required: "Seu nome é obrigatório"
				},
				'usuario.email': {
					required: "Seu e-mail é obrigatório",
					email: "E-mail inválido",
				}
			},
			errorPlacement: function(error, element) {
				var $element = $(element);
			    // verificar se existe mensagem de erro
			    if(!error.is(':empty')) {
			    	$element.filter(':not(.' + this.validClass + ')').qtip({
			            overwrite: false, // para o caso de já haver uma tooltip
			            content: error.text(),
			            position: {
			                my: 'left center',
			                at: 'right center'
			            },
			            show: {
			                event: false,
			                ready: true
			            },
			            hide: false,
			            style: {
			                classes: 'qtip-red qtip-rounded'
			            }
			        })
			        // atualiza o conteúdo caso já houver uma tooltip
			        .qtip('option', 'content.text', error.text());
			    } else {
			        // caso nao tenha mensagem de erro
			        // retira a tooltip que pode estar no elemento
			        $element.qtip('destroy');
			    }
			},
			// fix para bug da funcao errorPlacement nao dispararando
			success: $.noop
		});
	});
	</script>
</body>
</html>