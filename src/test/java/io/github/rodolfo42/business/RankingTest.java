package io.github.rodolfo42.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

/**
 * Teste da classe de negócio Ranking. Simula uma ordem de preferência do
 * usuário, faz os votos de acordo e verifica se Ranking 'adivinhou' a ordem.
 * 
 * @author rodolfo42
 */
public class RankingTest {

	/**
	 * Lista fixa de 5 de filmes para testes. Assim o teste fica independente do
	 * resto do código da aplicação, especificamente banco de dados.
	 */
	private final List<Filme> filmes = Arrays.asList(new Filme(
			"The Dark Knight"), new Filme("The Big Lebowski"), new Filme(
			"No Country For Old Men"), new Filme("La Vita è Bella"), new Filme(
			"Gran Torino"));

	/**
	 * Teste para um ranking cujo resultado seja na ordem que originalmente os
	 * filmes estão cadastrados.
	 */
	@Test
	public void testRankingSomenteEsquerdo() {
		Ranking rank = new Ranking(filmes);
		PerguntaRanking pergunta = null;
		while (!rank.isFinalizado()) {
			pergunta = rank.getPergunta();
			pergunta.responder(pergunta.getPrimeiroFilme());
		}

		ArrayList<Filme> ordem = rank.getResultado();

		assertEquals(filmes, ordem);
	}

	/**
	 * Teste para um ranking que resulte em uma ordem exatamente inversa á ordem
	 * que os filmes estão cadastrados originalmente.
	 */
	@Test
	public void testRankingSomenteDireito() {
		Ranking rank = new Ranking(filmes);
		PerguntaRanking pergunta = null;
		while (!rank.isFinalizado()) {
			pergunta = rank.getPergunta();
			pergunta.responder(pergunta.getSegundoFilme());
		}

		ArrayList<Filme> ordem = rank.getResultado();
		Collections.reverse(ordem);

		assertEquals(filmes, ordem);
	}

	/**
	 * Simulação de uma determinada preferência de filmes do usuário
	 */
	@Test
	public void testRankingEspecifico() {
		// simular uma ordem preferencia do usuario
		List<Filme> ordemPreferencia = new ArrayList<Filme>();
		ordemPreferencia.add(filmes.get(1));
		ordemPreferencia.add(filmes.get(0));
		ordemPreferencia.add(filmes.get(2));
		ordemPreferencia.add(filmes.get(4));
		ordemPreferencia.add(filmes.get(3));

		Ranking rank = new Ranking(filmes);
		PerguntaRanking pergunta = null;
		while (!rank.isFinalizado()) {
			pergunta = rank.getPergunta();

			// consultar na preferencia qual é o melhor e responder de acordo
			if (ordemPreferencia.indexOf(pergunta.getPrimeiroFilme()) < ordemPreferencia
					.indexOf(pergunta.getSegundoFilme())) {
				pergunta.responder(pergunta.getPrimeiroFilme());
			} else {
				pergunta.responder(pergunta.getSegundoFilme());
			}
		}

		assertEquals(ordemPreferencia, rank.getResultado());
	}
	
	
	@Test
	public void testResetRanking() {
		// simular uma ordem preferencia do usuario
		List<Filme> ordemPreferencia = new ArrayList<Filme>();
		ordemPreferencia.add(filmes.get(3));
		ordemPreferencia.add(filmes.get(4));
		ordemPreferencia.add(filmes.get(1));
		ordemPreferencia.add(filmes.get(2));
		ordemPreferencia.add(filmes.get(0));

		Ranking rank = new Ranking(filmes);
		PerguntaRanking pergunta = null;
		while (!rank.isFinalizado()) {
			pergunta = rank.getPergunta();

			// consultar na preferencia qual é o melhor e responder de acordo
			if (ordemPreferencia.indexOf(pergunta.getPrimeiroFilme()) < ordemPreferencia
					.indexOf(pergunta.getSegundoFilme())) {
				pergunta.responder(pergunta.getPrimeiroFilme());
			} else {
				pergunta.responder(pergunta.getSegundoFilme());
			}
		}
		
		rank.reset();
		assertFalse(rank.isFinalizado());
	}
}
