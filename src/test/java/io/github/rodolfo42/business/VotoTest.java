package io.github.rodolfo42.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import io.github.rodolfo42.business.Filme;
import io.github.rodolfo42.business.PerguntaRanking;
import io.github.rodolfo42.business.Ranking;
import io.github.rodolfo42.business.Usuario;
import io.github.rodolfo42.dao.FilmeDao;
import io.github.rodolfo42.dao.SessionFactoryProvider;
import io.github.rodolfo42.dao.UsuarioDao;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class VotoTest {
	
	private static SessionFactory factory;

	// DAOs usados nos testes
	private static FilmeDao filmeDao;
	private static UsuarioDao usuarioDao;
	
	private Session session;
	
	@BeforeClass
	public static void buildSessionFactory() {
		// TODO usar mesmo mecanismo de DI do vRator para pegar a instância da SessionFactory
		SessionFactoryProvider provider = new SessionFactoryProvider();
		provider.buildSessionFactory();
		factory = provider.getInstance();
	}
	
	@Before
	public void setupDao() {
		session = factory.openSession();
		filmeDao = new FilmeDao(session);
		usuarioDao = new UsuarioDao(session);
	}
	
	@After
	public void cleanDao() {
		if(session.isOpen()) {
			session.close();
		}
		
		filmeDao = null;
		usuarioDao = null;
	}
	
	@Test
	public void testRegistraUsuario() {
		Usuario usuario = new Usuario("John Doe", "johndoe@gmail");
		Integer id = usuarioDao.save(usuario);
		assertNotNull(id);
		
		Usuario usuarioPersistido = usuarioDao.getUsuario(id);
		assertNotNull(usuarioPersistido);
	}
	
	@Test
	public void testRegistraVoto() {
		ArrayList<Filme> filmes = filmeDao.getFilmes();
		
		// cria um ranking qualquer em cima dos 5 filmes
		Ranking ranking = new Ranking(filmes);
		PerguntaRanking pergunta = ranking.getPergunta();
		while(!ranking.isFinalizado()) {
			// consultar na preferencia qual é o melhor e responder de acordo
			if (filmes.indexOf(pergunta.getPrimeiroFilme()) < filmes.indexOf(pergunta.getSegundoFilme())) {
				pergunta.responder(pergunta.getPrimeiroFilme());
			} else {
				pergunta.responder(pergunta.getSegundoFilme());
			}
		}
		
		Usuario usuario = new Usuario("Mary Poppins", "mary.poppins@gmail");
		usuario.registrarRanking(ranking);
		usuarioDao.save(usuario);
		
		Filme filmePreferido = filmeDao.getFilmes().get(0);
		assertEquals(5, filmePreferido.getPosicaoRankingGeral());
	}
}
