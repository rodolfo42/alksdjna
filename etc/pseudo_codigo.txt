classe No
    var filho_direito
    var filho_esquerdo

classe Ranking

    var raiz
    var indice
    var pergunta_filme1
    var pergunta_filme2
    var filmes = []
    
    fn Construtor(films)
        filmes = films
        raiz = filmes[0]
        indice = 1
        prepara_pergunta(filmes[0], filmes[1])
    fimfn
    
    fn inserir(filme1, filme2, escolhido):
        se escolhido = filme2 entao
            se existe filho_direito(filme1) entao
                prepara_pergunta(filme2, filho_direito(filme1))
            senao
                filme1.filho_direito = filme2
                indice = indice+1
            fimse
        senao
            se existe filho_esquerdo(filme1) entao
                prepara_pergunta(filme2, filho_esquerdo(filme1))
            senao
                filme1.filho_esquerdo = filme2
                indice = indice+1
            fimse
        fimse
    fimfn
    
    fn filho_esquerdo(no):
        retorna no.filho_esquerdo
    fimfn
    
    fn filho_direito(no):
        retorna no.filho_direito
    fimfn
    
    fn pega_ordem(no):
        se no vazio entao
            no = raiz
        fimse
        
        ordem = []
        se existe filho_esquerdo(no) entao
            ordem.adiciona_todos(pega_ordem(filho_esquerdo(no)))
        senao
        
        ordem.adiciona(no)
        
        se existe filho_direito(no) entao
            ordem.adiciona_todos(pega_ordem(filho_direito(no)))
        fimse
        
        retorna ordem
    fimfn
    
    fn finalizou():
        retorna se existe filmes[indice]
    fimfn
    
    fn prepara_pergunta(filme1, filme2)
        pergunta_filme1 = filme1
        pergunta_filme2 = filme2
    fimfn
    
    fn pergunta():
        se finalizou() entao
            excecao "Ranking ja esta feito"
        senao
            retorna [pergunta_filme1, pergunta_filme2]
        fimse
    fimfn
    
    fn responde(opcao)
        se (opcao != pergunta_filme1) e (opcao != pergunta_filme2) entao
            excecao "Resposta nao consta"
        senao
            inserir( pergunta_filme1, pergunta_filme2, opcao )
        fimse
    fimfn
fimclasse

---------------------------------------------

novo Ranking(filmes)
enquanto nao ranking.finalizou:
    opcoes = ranking.pergunta()
    ranking.responde(opcao[1])
fimenquanto

exibir( ranking.pega_ordem )